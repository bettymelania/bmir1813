package agenda;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryContact;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@org.springframework.stereotype.Controller
public class Controller {

    RepositoryContact contactRep = new RepositoryContactFile();

    @GetMapping("/home")
    public String home() {
        return "homee";
    }

    @GetMapping("/addContact")
    public String addContact(Model model) {
        model.addAttribute("result", "");
        return "add_contact";
    }

    @PostMapping("/addContact")
    public String createContact(Model model, @ModelAttribute Contact contact) {
        contactRep.addContact(contact);
        model.addAttribute("result", "Added successFull");
        return "add_contact";
    }

}
