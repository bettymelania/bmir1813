package agenda.model.validator;

public class ContactValidator {

    public static boolean validName(String str)
    {
        return str.matches("[\\w\\-\\s.]{1,30}");
    }

    public static boolean validAddress(String adr)
    {
        String[] s = adr.split(",");
        if(s.length!=3)
            return false;
        if(s[0].isEmpty() || s[1].isEmpty() || s[2].isEmpty())
            return false;

        return true;
    }

    public static boolean validEmail(String email)
    {
        return email.matches("[a-z0-9_]+@[a-z]+\\.[a-z]+");
    }

    public static boolean validTelefon(String tel)
    {
       tel.matches("[[+0][0-9]{9,12}]+");
        return true;
    }
}
