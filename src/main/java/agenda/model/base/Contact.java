package agenda.model.base;

import agenda.exceptions.InvalidFormatException;
import agenda.model.validator.ContactValidator;

public class Contact {
	private String Name;
	private String Address;
	private String Telefon;
	private String Email;

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Contact(){
		Name = "";
		Address = "";
		Telefon = "";
		Email="";
	}
	
	public Contact(String name, String address, String telefon,String email) throws InvalidFormatException{
		if (!ContactValidator.validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		if (!ContactValidator.validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		if (!ContactValidator.validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
		if (!ContactValidator.validEmail(email)) throw new InvalidFormatException("Cannot convert", "Invalid email");
		Name = name;
		Address = address;
		Telefon = telefon;
		Email=email;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!ContactValidator.validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!ContactValidator.validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
		Address = address;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!ContactValidator.validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		Telefon = telefon;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=4) throw new InvalidFormatException("Cannot convert", "Invalid data");
		if (!ContactValidator.validTelefon(s[2])) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
		if (!ContactValidator.validName(s[0])) throw new InvalidFormatException("Cannot convert", "Invalid name");
		if (!ContactValidator.validAddress(s[1])) throw new InvalidFormatException("Cannot convert", "Invalid address");
		if (!ContactValidator.validEmail(s[3])) throw new InvalidFormatException("Cannot convert", "Invalid email");
		
		return new Contact(s[0], s[1], s[2],s[3]);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Name);
		sb.append("#");
		sb.append(Address);
		sb.append("#");
		sb.append(Telefon);
		sb.append("#");
		sb.append(Email);
		sb.append("#");
		return sb.toString();
	}

	
		
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (Name.equals(o.Name) && Address.equals(o.Address) &&
				Telefon.equals(o.Telefon) && Email.equals(o.Email))
			return true;
		return false;
	}
	
}
