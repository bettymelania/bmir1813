
package agenda.test;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "0798909890","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}
	

	
	@Test
	public void testCase2()
	{
		try {
			con = new Contact("", "loc,str,nr", "0798909890","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid name"));
		}

	}
	@Test
	public void testCase3()
	{
		try {
			con = new Contact("Maria", "loc", "0798909890","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid address"));
		}

	}

	@Test
	public void testCase4()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "0798909890","emaildomain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid email"));
		}

	}
	@Test
	public void testCase5()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "79890","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
		}

	}


	@Test
	public void testCase6()
	{
		try {
			con = new Contact("M", "loc,str,nr", "0798909890","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}
	@Test
	public void testCase7()
	{
		try {
			con = new Contact("MMMMMMMMMMMMMMMMMMMMMMMMMMMMM", "loc,str,nr", "0798909890","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}

	@Test
	public void testCase8()
	{
		try {
			con = new Contact("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM", "loc,str,nr", "0798909890","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}




	@Test
	public void testCase9()
	{
		try {
			con = new Contact("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM", "loc,str,nr", "0798909890","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid name"));
		}

	}

	@Test
	public void testCase10()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "079890989","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
		}

	}

	@Test
	public void testCase11()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "07989098900","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}

	@Test
	public void testCase12()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "+98909890099","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}

	@Test
	public void testCase13()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "+989098900999","email@domain.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		assertTrue(n+1 == rep.count());
	}


	@Test
	public void testCase14()
	{
		try {
			con = new Contact("Maria", "loc,str,nr", "+9890989009998","email@domain.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
		}

	}



}
